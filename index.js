function isPalindrome(str) {

      var rev = str.split('').reverse().join('');
      return str == rev;
    }
function SecondLongestPalindrome(str) {
      var array_palindrome = [];
      for (var i = 0; i < str.length; i++) {
        var sub_str = str.substr(i, str.length);

        for (var j = sub_str.length; j >= 0; j--) {
          var sub_sub_str = sub_str.substr(0, j);
          if(sub_sub_str.length>1)
          {
            if (isPalindrome(sub_sub_str)) {
                array_palindrome.push(sub_sub_str);
            }
          }
        }
      }
      if(array_palindrome.length==1)
      {
        return "No Second Palindrome exists";
      }
      else if(array_palindrome.length>1)
      {
      /*sort the palindrome array based on length, and second element in the array will be the second longest palindrome*/
      var secondlongest = array_palindrome.sort(function (a, b) { 			return b.length - a.length; })[1];
      	return "Found Palindrome: "+secondlongest;
      }
      else
      {
      	return "No Palindrome exists";
      }
      
      
    }
